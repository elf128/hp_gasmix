import QtQuick 2.0


Item {
    id: button
    property bool isDefined: true
    property int size: 32
    property int gap: size / 8

    width: size
    height: size

    MouseArea {
        id: mouse
        anchors.fill: parent
        onClicked: {
            parent.isDefined = !parent.isDefined
        }
    }

    Rectangle {
        id: checkArea
        color: "white"
        border.color: "#5f5f5f"
        width: parent.size - parent.gap * 2
        height: parent.size - parent.gap * 2
        x: parent.gap
        y: parent.gap
    }

    Image {
        id: iconNo
        source: "images/Def0.png"
        visible: ( parent.isDefined == false )
        x: 0
        y: 0
        height: parent.size
        width: parent.size
    }

    Image {
        id: iconYes
        source: "images/Def1.png"
        visible: ( parent.isDefined == true )
        x: 0
        y: 0
        height: parent.size
        width: parent.size
    }
/*
    Rectangle {
        id: checkbox
        width: 30
        height: 30
        border.color: "#999999"
        border.width: 1
        antialiasing: true
        radius: 2
        color: "transparent"
        Rectangle {
            anchors.fill: parent
            anchors.margins: 5
            antialiasing: true
            radius: 1
            color: mouse.pressed || buttonEnabled ? "#999999" : "transparent"
        }
    }*/
}


/*##^##
Designer {
    D{i:0;formeditorZoom:16}
}
##^##*/
