import QtQuick 2.15

Rectangle {
    id: prop
    color: "#60afafaf"
    //border.color: "#ffffff"
    implicitWidth: size * 6
    implicitHeight: size

    property alias defined: defCheck.isDefined
    property bool calculated: false
    property real value: 0.0
    property int size: 32

    property alias text: textInput.text

    signal userEnter( real newValue )

    DefineCheck {
        id: defCheck
        size: parent.size
        visible: !calculated
    }

    TextInput {
        id : textInput
        anchors {
            top: parent.top
            bottom: parent.bottom
            right: parent.right
        }

        text: value.toFixed(2).toString()
        enabled: ( ! calculated ) && defined

        color: ( calculated ) ? "#3f3f3f" : ( defined ? "#003f00" : "#3f0000" )
        font.pixelSize: parent.size - 4
        x: parent.height

        validator: DoubleValidator {
            bottom: 0
            notation: DoubleValidator.StandardNotation
        }

        onTextChanged: {
            if ( !calculated && acceptableInput )
            {
                console.log( "Property::onTextChanged: " + text )
                prop.userEnter( parseFloat( text ) )
            }
        }
    }

}

/*##^##
Designer {
    D{i:0;formeditorZoom:2;height:32;width:120}
}
##^##*/
