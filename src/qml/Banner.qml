

import QtQuick 2.0
import QtQuick.Layouts 1.1

Rectangle {
    id: header
    height: 64
    color: "#000000"

    RowLayout {
        anchors {
            fill: parent
            margins: 4
        }
        implicitWidth: 320

        Text {
            id: whiteText
            color: "#ffffff"
            font.family: "Sans"
            font.pointSize: 24
            text: "HP"
            horizontalAlignment: Text.AlignRight
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            //Layout.alignment: Qt.AlignRight
            //Layout.leftMargin: parent.width / 2.5
        }

        Text {
            id: redText
            color: "#ff1c1c"
            font.family: "Sans"
            font.pointSize: 24
            text: "GasMix"
            horizontalAlignment: Text.AlignHCenter
            Layout.rightMargin: 12
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            //horizontalAlignment: Text.AlignRight
            Layout.fillWidth: false
        }


        Image {
            id: props
            //anchors.right: parent.right
            source: "images/Props.png"
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            MouseArea {
                onClicked: root.showSettings = true
            }
        }



    }

}


