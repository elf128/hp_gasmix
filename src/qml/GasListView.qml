import QtQuick 2.0
import QtQml.Models 2.1
import QtQuick.Layouts 1.1

Rectangle {
    id: table
    color: "#e0e0e0"
    //anchors.fill: parent

    property int mainScale: width / 20

    //Strings
    property string underButton:      "Underconstrained"
    property string underConstrained: "You have too many undefined fields to find solution"
    property string overButton:       "Overconstrained"
    property string overConstrained:  "You need more undefined fields to find solution"
    property string rightButton:      "Solve"
    property string rightConstrained: "Right amount of undefined fields"

    //property alias currentIndex: root.currentIndex
    ListModel {
        id: sampleModel
        ListElement { pres_value: 50;  pres_def: true;  fO2_value: 0.28; fO2_def: true;  fHe_value: 0.0; fHe_def: true  }
        ListElement { pres_value: 0;   pres_def: false; fO2_value: 0.0;  fO2_def: false; fHe_value: 0.0; fHe_def: true  }
        ListElement { pres_value: 0;   pres_def: true;  fO2_value: 0.0;  fO2_def: true;  fHe_value: 0.0; fHe_def: true  }
        ListElement { pres_value: 210; pres_def: true;  fO2_value: 0.32; fO2_def: true;  fHe_value: 0.0; fHe_def: true  }
    }

    Component {
        id: gasLine
        GasLine {
            size: table.mainScale
            itemCount: _gasModel.gasCount
            itemModel: model
        }
    }

    ColumnLayout {
        anchors.fill: parent

        ListView {
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop
            Layout.fillWidth: true
            Layout.fillHeight: true
            //Layout.preferredHeight: _listModel.count() * parent.mainScale

            model: _listModel
            //model: _gasModel
            delegate: gasLine
        }

        Rectangle {
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop
            Layout.fillWidth: true
            //Layout.fillHeight: true

            implicitHeight: table.mainScale

            Rectangle {
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                width: ( parent.width > parent.mainScale*10 ) ? parent.mainScale*10 : parent.width;
                height: parent.height - 4

                color: "grey"

                Text {
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    text: ( parent.undefCount < parent.undefLimit ) ? table.overButton : (
                          ( parent.undefCount > parent.undefLimit ) ? table.underButton  : table.rightButton )
                    font.pixelSize: table.mainScale
                }

                MouseArea {
                    id: mouse
                    anchors.fill: parent
                    onClicked: {
                        _gasModel.solve()
                    }
                }
            }
        }
    }
}



/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
