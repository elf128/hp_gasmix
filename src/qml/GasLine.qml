import QtQuick 2.15
import QtQuick.Layouts 1.1


Rectangle {
    id: line
    property var itemModel: ({})

    color: isSource ? "#cf9f9f" :( isTarget ? "#9fb09f" : "#afafaf" )
    property int itemIdx: itemModel.index
    property int itemCount: 0

    property bool isSource: itemIdx == 0
    property bool isTarget: itemIdx + 1 == itemCount

    property int size: 32

    anchors.left: parent.left
    anchors.right: parent.right
    height: size
    implicitWidth: 120

    RowLayout {
        //var totalValue = itemModel.pres_value;

        Property {
            id: total
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop
            Layout.fillWidth: true

            value: itemModel.pres_value
            defined: itemModel.pres_def
            size: line.size;

            onValueChanged: {
                console.log( "total.onValueChanged: " + value )
            }

            onDefinedChanged: {
                itemModel.pres_def = defined
            }

            onUserEnter: {
                console.log( "total.onUserEnter: " + newValue )
                _gasModel.userSetTotal( newValue, itemIdx, 0 )
            }
        }

        Property {
            id: fO2
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop

            value: itemModel.fO2_value
            defined: itemModel.fO2_def
            size: line.size;


            onValueChanged: {
                console.log( "fO2.onValueChanged: " + value )
            }

            onDefinedChanged: {
                itemModel.fO2_def = defined
            }

            onUserEnter: {
                console.log( "fO2.onUserEnter: " + newValue )
                _gasModel.userSetTotal( newValue, itemIdx, 1 )
            }
        }

        Property {
            id: ppO2
            Layout.alignment: Qt.AlignRight | Qt.AlignTop

            value: itemModel.ppO2_value
            defined: false
            calculated: true
            size: line.size;

            onValueChanged: {
                console.log( "ppO2.onValueChanged: " + value )
            }

        }
    }

    //Component.onCompleted: console.log( "id: " + itemIdx )
}
