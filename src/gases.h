#ifndef GASES_H
#define GASES_H

#include <QObject>
#include <QAbstractListModel>
#include <QString>

class Gas : public QObject
{
    Q_OBJECT
    Q_PROPERTY(double pres_value READ getTotal    NOTIFY totalIsChanged  )
    Q_PROPERTY(bool   pres_def   READ getTotalDef WRITE setTotalDef NOTIFY totalDefChanged )

    Q_PROPERTY(double fO2_value   READ getFO2     NOTIFY fO2IsChanged    )
    Q_PROPERTY(bool   fO2_def     READ getFO2Def  WRITE setFO2Def NOTIFY fO2DefChanged   )
    Q_PROPERTY(double ppO2_value  READ getPpO2    NOTIFY ppO2Changed     )

    Q_PROPERTY(double fHe_value   READ getFHe     NOTIFY fHeIsChanged    )
    Q_PROPERTY(bool   fHe_def     READ getFHeDef  WRITE setFHeDef NOTIFY fHeDefChanged   )
    Q_PROPERTY(double ppHe_value  READ getPpHe    NOTIFY ppHeChanged     )


public:
    Gas( double total, double fO2, double fHe );
    Gas( double fO2, double fHe );
    Gas() : Gas( 0.32, 0.0 ) {}
    Gas( const Gas& gas );

    void setTotal( double value, const bool override = 0 );
    void setFO2( double value, const bool override = 0 );
    void setFHe( double value, const bool override = 0 );

    void setTotalDef( bool value );
    void setFO2Def( bool value );
    void setFHeDef( bool value );

    double getTotal() const;
    double getPpO2() const;
    double getPpHe() const;
    double getFO2() const;
    double getFHe() const;

    bool getTotalDef() const;
    bool getFO2Def() const;
    bool getFHeDef() const;

    operator const QString() const;

signals:
    void totalIsChanged();
    void fO2IsChanged();
    void ppO2Changed();
    void fHeIsChanged();
    void ppHeChanged();

    void totalDefChanged();
    void fO2DefChanged();
    void fHeDefChanged();
    void definitionChanged();

//    void def
private:
    double m_total;
    double m_ppO2;
    double m_ppHe;

    double m_fO2_cache;
    double m_fHe_cache;

    bool m_isDef_Total;
    bool m_isDef_fO2;
    bool m_isDef_fHe;

    uint8_t _pad[5];
};

template<int equations> struct GasVariable;

class GasFill : public QObject
{
    Q_OBJECT
    Q_PROPERTY( int     gasCount       READ   gasCount     NOTIFY gasCountChanged    )
    Q_PROPERTY( int     undefinedCount MEMBER m_undefined  NOTIFY undefinedChanged   )
    Q_PROPERTY( bool    solveRes       MEMBER m_solveRes   NOTIFY solveStringChanged )
    Q_PROPERTY(QString  solveText      MEMBER m_solveText  NOTIFY solveStringChanged )

public:
    enum class Type
    {
        Const,
        Total,
        PartialO2,
        PartialHe
    };

    int        gasCount() const;
    void       addGas( Gas* gas );
    const Gas* getGas( int row ) const { return static_cast<Gas*>( m_listModel[row] ); }
    QVariant   getModel() { return QVariant::fromValue( m_listModel ); }

public slots:
    void refreshDefinitions();
    void solve();
    void userSetTotal( double value, int gasIdx, int fieldIdx );


    //void addGas( int row, qreal fO2, qreal fHe );
    //void removeGas( int row );
    //void updateList();

signals:
    void gasCountChanged();
    void undefLimitChanged();
    void undefinedChanged();
    void solveStringChanged();

private:
    template<int equations>
    bool buildVars( QList<GasVariable<equations>>& vars, GasVariable<equations>& consts );

    template<int equations>
    bool buildMatrix( const QList<GasVariable<equations>>& vars, QMatrix4x4& resM );

    template<int equations>
    void buildVector( const GasVariable<equations> consts, QVector4D& resV );

    template<int equations>
    void storeSolution( const QList<GasVariable<equations>>& vars, const QMatrix4x4& invM, const QVector4D& resV );

    template<int equations>
    void debugVars( const QList<GasVariable<equations>>& vars, const GasVariable<equations>& consts );

    QList<QObject*> m_listModel;
    QString         m_solveText;

    int  m_undefined  = 0;
    bool m_solveRes = false;
};

template<int equations>
struct GasVariable
{

    GasVariable( const GasFill::Type type, const int gasIdx, const double scale[equations],  const int varIdx )
        : m_varIdx( varIdx ), m_gasIdx( gasIdx ), m_type( type )
    { for(int i = 0; i < equations; i++) m_scale[i] = scale[i]; }

    GasVariable( const GasFill::Type type, const int eqIdx, const int gasIdx, const double scale,  const int varIdx )
        : m_varIdx( varIdx ), m_gasIdx( gasIdx ), m_type( type )
    { for(int i = 0; i < equations; i++) m_scale[i] = ( i == eqIdx ) ? scale : 0.0; }

    GasVariable()
        : GasVariable( GasFill::Type::Const, 0, -1, 0.0, -1 ) {}

    double add( const int eqIdx, const double value ) { m_scale[eqIdx] += value; return m_scale[eqIdx]; }

    double m_scale[equations];
    int   m_varIdx;
    int   m_gasIdx;
    GasFill::Type  m_type;
};

using Gas2Variable = GasVariable<2>;
using Gas3Variable = GasVariable<3>;
using Gas4Variable = GasVariable<4>; // Possible room for extra gas. May be hydrogen.

#endif // GASES_H
