#include "gases.h"
#include <math.h>
#include <QDebug>

#include <QMatrix4x4>
#include <QVector4D>

const double epilon = 0.000001;

inline bool same( double a, double b )
{
    return fabs( a - b ) < epilon;
}

Gas::Gas( double total, double fO2, double fHe )
    : QObject()
    , m_total( total )
    , m_fO2_cache( -1 )
    , m_fHe_cache( -1 )
    , m_isDef_Total( true )
    , m_isDef_fO2( true )
    , m_isDef_fHe( true )

{
    setFO2( fO2 );
    setFHe( fHe );
}

Gas::Gas( double fO2, double fHe )
    : QObject()
    , m_total( 0.0 )
    , m_ppO2( 0.0 )
    , m_ppHe( 0.0 )
    , m_fO2_cache( fO2 )
    , m_fHe_cache( fHe  )
    , m_isDef_Total( false )
    , m_isDef_fO2( true )
    , m_isDef_fHe( true )

{
}

Gas::Gas( const Gas& gas )
    : QObject()
    , m_total( gas.m_total )
    , m_ppO2(  gas.m_ppO2 )
    , m_ppHe(  gas.m_ppHe )
    , m_fO2_cache( gas.m_fO2_cache )
    , m_fHe_cache( gas.m_fHe_cache )
    , m_isDef_Total( gas.m_isDef_Total )
    , m_isDef_fO2( gas.m_isDef_fO2 )
    , m_isDef_fHe( gas.m_isDef_fHe )

{
}

void Gas::setTotal( double value, const bool override )
{
    if (( m_isDef_Total || override ) && !same( m_total, value ) )
    {
        m_total = value;
        m_ppO2  = value * m_fO2_cache;
        m_ppHe  = value * m_fHe_cache;

        if ( override )
        {
            emit totalIsChanged();
        }

        emit ppO2Changed();
        emit ppHeChanged();
    }
}

void Gas::setFO2( double value, const bool override )
{
    if (( m_isDef_fO2 || override ) && !same( m_fO2_cache, value ) )
    {
        m_fO2_cache = value;
        m_ppO2  = m_total * m_fO2_cache;

        if ( override )
        {
            emit fO2IsChanged();
        }

        emit ppO2Changed();
    }
}

void Gas::setFHe( double value, const bool override )
{
    if (( m_isDef_fHe || override ) && !same( m_fHe_cache, value ) )
    {
        m_fHe_cache = value;
        m_ppHe  = m_total * m_fHe_cache;

        if ( override )
        {
            emit fHeIsChanged();
        }

        emit ppHeChanged();
    }
}

void Gas::setTotalDef( bool value )   {  m_isDef_Total = value; emit definitionChanged();  }
void Gas::setFO2Def( bool value )     {  m_isDef_fO2   = value; emit definitionChanged();  }
void Gas::setFHeDef( bool value )     {  m_isDef_fHe   = value; emit definitionChanged();  }

double Gas::getTotal() const {    return m_total;          }
double Gas::getPpO2() const  {    return m_ppO2;           }
double Gas::getPpHe() const  {    return m_ppHe;           }
double Gas::getFO2() const   {    return m_fO2_cache;      }
double Gas::getFHe() const   {    return m_fHe_cache;      }
bool Gas::getTotalDef() const {   return m_isDef_Total;    }
bool Gas::getFO2Def() const   {   return m_isDef_fO2;      }
bool Gas::getFHeDef() const   {   return m_isDef_fHe;      }

Gas::operator const QString() const
{
    return QString("%1 = O2( %2 ) + He( %3 )").arg(m_total).arg(m_ppO2).arg(m_ppHe);
}


/*********************************************************
 *
 *
 *
**********************************************************/


int GasFill::gasCount() const
{
    return m_listModel.count();
}

void GasFill::addGas( Gas* gas )
{
    const int gasCount = m_listModel.count();

    m_listModel.insert( gasCount, gas );

    connect( gas, &Gas::definitionChanged, this, &GasFill::refreshDefinitions );

    refreshDefinitions();
}

void GasFill::refreshDefinitions()
{
    int undef = 0;
    foreach( QObject* obj, m_listModel )
    {
        Gas* gas = static_cast<Gas*>(obj);
        //qDebug() << gas[0];

        if ( !gas->getTotalDef() )
            undef++;

        if ( !gas->getFO2Def() )
            undef++;

        if ( !gas->getFHeDef() )
            undef++;
    }

    if ( undef != m_undefined )
    {
        m_undefined = undef;
        emit undefinedChanged();
        solve();
    }
}

void GasFill::userSetTotal( double value, int gasIdx, int fieldIdx )
{
    qDebug() << value << gasIdx;
    if ( gasIdx >= 0 && gasIdx < m_listModel.count() )
    {
        Gas* gas = static_cast<Gas*>( m_listModel[ gasIdx ] );
        switch( fieldIdx )
        {
            case 0:
                gas->setTotal( value );
                break;
            case 1:
                gas->setFO2( value );
                break;
            case 2:
                gas->setFHe( value );
                break;
        }
        solve();
    }
}

template<int equations>
bool GasFill::buildVars( QList<GasVariable<equations>>& vars, GasVariable<equations>& consts )
{
    int varIdx = 0;

    for( int i = 0; i < m_listModel.count(); i++ )
    {
        Gas* gas = static_cast<Gas*>( m_listModel[ i ] );

        const bool defTotal = gas->getTotalDef();
        const bool defFO2   = gas->getFO2Def();
        const bool defFHe   = gas->getFHeDef();

        const bool isResult = ( i + 1 ) == m_listModel.count();

        const double s = ( isResult ) ? -1.0 : 1.0;

        const double t  = gas->getTotal() * s;
        const double fO = gas->getFO2() * s;
        const double pO = gas->getPpO2() * s;
        const double fH = gas->getFHe() * s;
        const double pH = gas->getPpHe() * s;

        if ( !defFO2 )
            vars.append( GasVariable<equations>( GasFill::Type::PartialO2, 1, i, s, varIdx++ ) );

        if ( equations>2 && !defFHe )
            vars.append( GasVariable<equations>( GasFill::Type::PartialHe, 2, i, s, varIdx++ ) );

        if ( !defTotal )
        {
            double C[equations];

            C[0] = s;
            C[1] = (( equations>1 ) && defFO2 ) ? fO : 0.0;
            C[2] = (( equations>2 ) && defFHe ) ? fH : 0.0;
            for(int i = 3; i < equations; i++) C[i] = 0.0;

            vars.append( GasVariable<equations>( GasFill::Type::Total, i, C, varIdx++ ) );

        }
        else
        {
            consts.add( 0, t );

            if ( defFO2 )
                consts.add( 1, pO );

            if ( defFHe )
                consts.add( 2, pH );
        }
    }

    return ( varIdx < equations );
}

template<int equations>
void GasFill::debugVars( const QList<GasVariable<equations>>& vars, const GasVariable<equations>& consts )
{
    QString eq[3];

    foreach( const Gas3Variable& v, vars )
    {
        //QString &e = eq[ v.m_eqIdx ];
        QString name;
        QString pad = "      0      + ";
        switch(v.m_type)
        {
            case GasFill::Type::Total:     name = "%1*t%2[%3] + "; break;
            case GasFill::Type::PartialO2: name = "%1*p%2[%3] + "; break;
            case GasFill::Type::PartialHe: name = "%1*h%2[%3] + "; break;
            default:                       name = "%1_%2[%3]";
        }

        eq[0] += ( v.m_scale[0] != 0.0 ) ? name.arg( v.m_scale[0], 6, 'f', 2, ' ' ).arg( v.m_gasIdx ).arg( v.m_varIdx ) : pad;
        eq[1] += ( v.m_scale[1] != 0.0 ) ? name.arg( v.m_scale[1], 6, 'f', 2, ' ' ).arg( v.m_gasIdx ).arg( v.m_varIdx ) : pad;
        eq[2] += ( v.m_scale[2] != 0.0 ) ? name.arg( v.m_scale[2], 6, 'f', 2, ' ' ).arg( v.m_gasIdx ).arg( v.m_varIdx ) : pad;
    }

    eq[0] += QString("= %1").arg( -consts.m_scale[0] );
    eq[1] += QString("= %1").arg( -consts.m_scale[1] );
    eq[2] += QString("= %1").arg( -consts.m_scale[2] );


    qDebug() << eq[0];
    qDebug() << eq[1];
    qDebug() << eq[2];

}

template<int equations>
bool GasFill::buildMatrix( const QList<GasVariable<equations>>& vars, QMatrix4x4& resM )
{
    QMatrix4x4 M;

    M.fill( 0.0 );
    foreach( const GasVariable<equations>& v, vars )
    {
        QVector4D col = M.column( v.m_varIdx );

        for( int i = 0; i < equations; i++ )
            col[ i ] = v.m_scale[i];

        M.setColumn( v.m_varIdx, col );
    }

    for( int i = vars.count(); i < 4; i++ )
    {
        QVector4D col = M.column( i );
        col[ i ] = 1.0;
        M.setColumn( i, col );
    }

    qDebug() << M.row(0);
    qDebug() << M.row(1);
    qDebug() << M.row(2);
    qDebug() << M.row(3);
    qDebug() << "-----";

    bool inv = false;
    resM = M.inverted( &inv );

    return inv;
}

template<int equations>
void GasFill::buildVector( const GasVariable<equations> consts, QVector4D& resV )
{
    resV = QVector4D(
        ( equations > 0 ) ? -consts.m_scale[0] : 0.0,
        ( equations > 1 ) ? -consts.m_scale[1] : 0.0,
        ( equations > 2 ) ? -consts.m_scale[2] : 0.0,
        ( equations > 3 ) ? -consts.m_scale[3] : 0.0);
}

template<int equations>
void GasFill::storeSolution( const QList<GasVariable<equations>>& vars, const QMatrix4x4& invM, const QVector4D& resV )
{
    qDebug() << resV;
    qDebug() << "-----";

    qDebug() << invM.row(0);
    qDebug() << invM.row(1);
    qDebug() << invM.row(2);
    qDebug() << invM.row(3);
    qDebug() << "-----";

    QVector4D res = invM * resV;
    qDebug() << res;

    QString stringLog;
    for( int pass = 0; pass < 2; pass++ )
    {
        foreach( const GasVariable<equations>& v, vars )
        {
            const int gasIdx = v.m_gasIdx;
            const int varIdx = v.m_varIdx;
            Q_ASSERT( varIdx < 4 );

            Gas* gas = static_cast<Gas*>( m_listModel[ gasIdx ] );

            if ( pass == 0 && v.m_type == GasFill::Type::Total )
            {
                 const double t = double( res[ varIdx ] );
                 gas->setTotal( t, true );
                 qDebug() << QString("Total%1 = %2").arg(gasIdx).arg(t);
                 stringLog += QString("Total%1 = %2\n").arg(gasIdx).arg(t);
            }

            if ( pass == 1 && v.m_type != GasFill::Type::Total )
            {
                double f = double( res[ varIdx ] ) / gas->getTotal();

                switch(v.m_type)
                {
                    case GasFill::Type::PartialO2:
                        gas->setFO2( f, true );
                        qDebug() << QString("fO%1 = %2").arg(gasIdx).arg(f);
                        stringLog += QString("fO%1 = %2\n").arg(gasIdx).arg(f);
                        break;
                    case GasFill::Type::PartialHe:
                        gas->setFHe( f, true );
                        qDebug() << QString("fHe%1 = %2").arg(gasIdx).arg(f);
                        stringLog += QString("fHe%1 = %2\n").arg(gasIdx).arg(f);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    m_solveText = stringLog;
    m_solveRes = true;
    emit solveStringChanged();
}

void GasFill::solve()
{
    qDebug() << "Solve";

    QList<Gas3Variable> vars;
    vars.reserve( 3 );
    Gas3Variable consts;

    if( buildVars<3>( vars, consts ) )
    {
        debugVars<3>( vars, consts );

        //if ( varIdx != 3 )
        QMatrix4x4 M;
        if ( buildMatrix<3>( vars, M ) )
        {
            QVector4D R;
            buildVector( consts, R );

            storeSolution( vars, M, R );
        }
        else
        {
            qDebug() << "Matrix has no inverse. Cannot solve";
            m_solveText = "Matrix has no inverse. Cannot solve";
            m_solveRes = false;
            emit solveStringChanged();
        }
    }
    else
    {
        qDebug() << "More variables then equations. Cannot solve";
        m_solveText = "More variables then equations. Cannot solve";
        m_solveRes = false;
        emit solveStringChanged();
    }
}

/*
void GasFill::addGas( int row, qreal fO2, qreal fHe )
{
    if ( row <= 0 || ( row + 1 ) >= m_gases.count() )
        return;

    const Gas gas( fO2, fHe );
    //beginInsertRows( QModelIndex(), row, row );
    m_gases.insert( row, gas );
    //endInsertRows();
}

void GasFill::removeGas( int row )
{
    if ( row <= 0 || ( row + 1 ) >= m_gases.count() )
        return;

    //beginRemoveRows( QModelIndex(), row, row );
    m_gases.removeAt( row );
    //endRemoveRows();
}

void GasFill::updateList()
{
    m_listModel.clear();
    for( Gas& gas : m_gases )
    {
        m_listModel.append( &gas );
    }
}*/


