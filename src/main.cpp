#include <QGuiApplication>
#include <QQmlEngine>
#include <QQmlFileSelector>
#include <QQmlContext>
#include <QQuickView>
#include <QDebug>
#include <QList>

#include "gases.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setOrganizationName("Vlad A.");

    Gas startGas = Gas( 40, 0.32, 0.0 );
    Gas gasToAdd = Gas( 0.32, 0.0 );
    gasToAdd.setFO2Def( false );
    Gas endGas = Gas( 207, 0.32, 0.0 );

    GasFill gasFill;

    //QList<QObject*> listModel;
    gasFill.addGas( &startGas );
    gasFill.addGas( &gasToAdd );
    gasFill.addGas( &endGas   );

    qDebug() << gasFill.getGas( 0 )[0];
    qDebug() << gasFill.getGas( 1 )[0];
    qDebug() << gasFill.getGas( 2 )[0];

    QGuiApplication app(argc, argv);

    QQuickView view;

    QQmlContext* context = view.engine()->rootContext();
    context->setContextProperty("_listModel", gasFill.getModel() );
    context->setContextProperty("_gasModel", QVariant::fromValue( &gasFill ) );

    view.connect( view.engine(), &QQmlEngine::quit, &app, &QCoreApplication::quit );
    view.setSource(QUrl("qrc:/hp_gasmix.qml"));

    if (view.status() == QQuickView::Error)
        return -1;

    view.setResizeMode( QQuickView::SizeRootObjectToView );
    view.show();

    return app.exec();
}
