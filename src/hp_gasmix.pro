QT += qml quick

SOURCES += \
    gases.cpp \
    main.cpp

RESOURCES += \
    hp_gasmix.qrc
    
OTHER_FILES += \
        *.qml \
        qml/*.qml \
        qml/images/*.png


#target.path = $$[QT_INSTALL_EXAMPLES]/charts/datetimeaxis
INSTALLS += target

DISTFILES += \
    android/AndroidManifest.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew \
    android/gradlew.bat \
    android/res/values/libs.xml

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

HEADERS += \
    gases.h
